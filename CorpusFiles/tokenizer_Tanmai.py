import os
import re
from collections import Counter
from plotly import __version__
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
from plotly.graph_objs import *

def tokenise(text):
	#PREPROCESSING

	#Adding spaces around punctuations and brackets (except .')
	text = re.sub(r',', ' , ', text) 
	text = re.sub(r'\(', ' ( ', text)
	text = re.sub(r'\)', ' ) ', text)
	text = re.sub(r'\[', ' [ ', text)
	text = re.sub(r'\]', ' ] ', text)
	text = re.sub(r'\{', ' { ', text)
	text = re.sub(r'\}', ' } ', text)
	text = re.sub(r'\&', ' & ', text)
	text = re.sub(r'\*', ' * ', text)
	text = re.sub(r'\%', ' % ', text)
	text = re.sub(r'\<', ' < ', text)
	text = re.sub(r'\>', ' > ', text)
	text = re.sub(r'\"', ' " ', text)
	text = re.sub(r'\:', ' : ', text)
	text = re.sub(r'\;', ' ; ', text)
	text = re.sub(r'\|', ' | ', text)
	text = re.sub(r'\/', ' / ', text)
	text = re.sub(r'\_', ' _ ', text)
	text = re.sub(r'\+', ' + ', text)
	text = re.sub(r'\-', ' - ', text) #Hyphenated words are also individually words (usually)
	text = re.sub(r'\^', ' ^ ', text)
	text = re.sub(r'\!', ' ! ', text)
	text = re.sub(r'\?', ' ? ', text)
	text = re.sub(r'\=', ' = ', text)
	text = re.sub(r'\$', ' $ ', text)
	text = re.sub(r'\@', ' @ ', text)
	text = re.sub(r'\~', ' ~ ', text)
	text = re.sub(r'\#', ' # ', text)
	
	#Remove all whitespace in the text and replace it with spaces. (Cannot trust current line divisions)
	#Also replace all trailing whitespace with a single space
	text = re.sub(r'[\s]+', ' ', text) #Replaces all whitespace occuring one to infinite times with a single space

	#Sentence tokenisation
	'''
	Regex explanation:
	Replace each space with a \n whenever all these are true:
	1. Preceding character IS a ! or a ? or a .
	2. Preceding word IS NOT Mr., Jr., etc. or the like
	3. Preceding word IS NOT Mrs. or the like
	4. Preceding word IS NOT A., B. or the like (Initials - for eg., A. Lincoln) Only one capital letter preceding by a space
	5. Preceding word IS NOT A.B.C or X.Y or 9.6 or the like
	'''
	text = re.sub(r'(?<=\.|\!|\?)(?<![A-Z][a-z]\.)(?<![A-Z][a-z][a-z]\.)(?<![ ][A-Z].)(?<!\w\.\w.)\s','\n', text)

	text = re.sub(r'\.\n', ' .\n', text) #Add a space before ending period

	text = text.lower() #Making all characters lowercase - Note that this is done after the tokenisation is done

	sentence_tokens = text.split('\n')

	tokenised_data = []
	sentence_count = -1

	for sentence in sentence_tokens:
		sentence_count = sentence_count + 1
		tokenised_data.append([]) #Adding empty array in final tokenised data array

		tokenised_data[sentence_count] = sentence
		tokenised_data[sentence_count] = tokenised_data[sentence_count].split(' ') #Splitting words by spaces

	#This is a 2D array with elements as sentences which are arrays with elements as words
	return tokenised_data

	###End of Tokeniser function

def language_model_spellcheck(tokenised):
	word_tokens = []

	for sentence in tokenised:
		for word in sentence:
			word_tokens.append(word)

	token_counts = Counter(word_tokens)

	#print(token_counts)

	#Plot a graph

	#LM for Spelling Correction
	#Dividing data into trigrams, bigrams and unigrams

	trigrams = []

	for word in word_tokens:
		count = 0
		while(len(word) - count >= 3):
			trigrams.append(word[count:count+3])
			count = count + 1

	trigrams_count = Counter(trigrams)
	#print(trigrams_count)

	bigrams = []

	for word in word_tokens:
		count = 0
		while(len(word) - count >= 2):
			bigrams.append(word[count:count+2])
			count = count + 1

	bigrams_count = Counter(bigrams)
	#print(bigrams_count)

	unigrams = []

	for word in word_tokens:
		count = 0
		while(len(word) - count >= 1):
			unigrams.append(word[count:count+1])
			count = count + 1

	unigrams_count = Counter(unigrams)
	#print(unigrams_count)

	n_grams = [unigrams_count, bigrams_count, trigrams_count] #Putting all in one variable
	return n_grams

	###End of Language Model function

def language_model_grammar(tokenised):
	trigrams = []
	#LM for Grammar Check
	for sentence in tokenised:
		count = 0
		while(len(sentence) - count >=3):
			trigrams.append(sentence[count:count+3])
			count = count + 1

	#print(trigrams)

	trigrams_new = []

	#We want to put all three words in a trigram in one string instead of separate strings for easy counting
	for trigram in trigrams:
		trigrams_new.append('~'.join(trigram))

	#print(trigrams_new)
	
	trigrams_count = Counter(trigrams_new)
	#print(trigrams_count)

	bigrams = []
	for sentence in tokenised:
		count = 0
		while(len(sentence) - count >=2):
			bigrams.append(sentence[count:count+2])
			count = count + 1

	bigrams_new = []

	for bigram in bigrams:
		bigrams_new.append('~'.join(bigram))

	bigrams_count = Counter(bigrams_new)
	#print(bigrams_count)

	unigrams = []
	for sentence in tokenised:
		for word in sentence:
			unigrams.append(word)

	unigrams_count = Counter(unigrams)
	#print(unigrams_count)

	n_grams = [unigrams_count, bigrams_count, trigrams_count] #Putting all in one variable
	return n_grams

	###End of Language Model function

def no_smoothing_spellcheck(input_trigrams, n_grams):
	#Calculating probabilities without any smoothing

	#Getting ngram counts
	bigrams_count = n_grams[1]
	trigrams_count = n_grams[2]

	#Calculating Probabilities
	probability = float(1)
	for trigram in input_trigrams:
		
		#We need P(char3|char1,char2) = Count(char3|char1,char2) / Count(char1,char2)

		#print(trigrams_count[trigram])
		#print(bigrams_count[trigram[0:2]])
		probability_trigram = float(trigrams_count[trigram]) / float(bigrams_count[trigram[0:2]])
		probability = probability * probability_trigram

	return probability

	###End of No Smoothing Function

def laplace_smoothing_spellcheck(input_trigrams, n_grams):
	#We need Vocabulary Size(V), i.e. number of possible trigrams
	
	unigrams_count = n_grams[0]
	bigrams_count = n_grams[1]
	trigrams_count = n_grams[2]
	
	C = len(unigrams_count) #Note that the characters here will also include punctuations etc. which we can assume
							#will not be present in the input but this makes for a more general solution
	#print(C)

	V = float(C*C*C)				#This is the number of all possible trigrams with our available characters

	#Calculating probabilities with Laplace Smoothing

	probability = float(1)
	
	for trigram in input_trigrams:
		
		#We need P(char3|char1,char2) = Count(char3|char1,char2) + 1 / Count(char1,char2) + V

		probability_trigram = (float(trigrams_count[trigram]) + 1) / (float(bigrams_count[trigram[0:2]]) + V)
		probability = probability * probability_trigram

	return probability

	###End of Laplace Smoothing function

def eq_classes(ngrams):
	#Function to get Nrs from Ngrams
    eq = {}
    for k, v in ngrams.items():
        if v not in eq:
            eq[v] = []
        eq[v].append(k)
    return eq

    ###End of Eq Classes Function


def good_turing(n_grams_count, k):
	#Function to Calculate New Nrs

	trigrams_count = n_grams_count[2]
	unigrams_count = n_grams_count[0]

	#print(trigrams_count)
	N = sum(list(trigrams_count.values()))

	nrs = eq_classes(trigrams_count)
	#print(nrs)
	nr_counts = {u : len(v) for u, v in nrs.items()}
	
	new_probs = {}

	for r in nr_counts:
		if r <= k:
			#Formula to calculate c* for 1<=c<=k is given in the slides
			new_r = (((r+1) * nr_counts[r+1]) / float(nr_counts[r])) - ((r*(k+1)*nr_counts[k+1]) / float(nr_counts[1]))
			new_r = new_r / (1 - (((k+1)*nr_counts[k+1])/float(nr_counts[1])))
		else:
			new_r = r
			#break

		new_probs[r] = new_r / float(N)

	C = float(len(unigrams_count))
	V = C*C*C #All possible trigrams
	
	C_trigram = float(len(trigrams_count)) 
	V = V - C_trigram #Number of unseen trigrams
	
	new_probs[0] = nr_counts[1] / float(N)
	new_probs[0] = new_probs[0] / float(V) #Need to divide probability mass among all unseen examples
	
	#print(sorted(nr_counts.items()))
	print(sorted(new_probs.items()))
	return new_probs

	### End of Good Turing Calculation Function
	
def good_turing_smoothing_spellcheck(input_trigrams, n_grams):

	k = 6

	trigrams_count = n_grams[2]
	bigrams_count = n_grams[1]

	new_probs = good_turing(n_grams, k) #Need to give k above which GTcounts will be the same

	probability = float(1)
	
	for trigram in input_trigrams:
		print(trigram)

		count_trigram = trigrams_count[trigram]

		GTprob = new_probs[count_trigram]

		#print(GTprob)
		probability = probability * GTprob;

	print(probability)
	return probability

	###End of Good Turing Spellcheck Function


def good_turing_backoff(n_grams_count, k):
	#Function to Calculate New Nrs

	#print(trigrams_count)
	N = sum(list(n_grams_count.values()))

	nrs = eq_classes(n_grams_count)
	#print(nrs)
	nr_counts = {u : len(v) for u, v in nrs.items()}
	
	new_counts = {}
	for r in nr_counts:
		#if (r+1) in nr_counts:
		if r <= k:
			new_r = ((r+1) * nr_counts[r+1]) / float(nr_counts[r])
		else:
			new_r = float(r)
			#break

		new_counts[r] = new_r

	
	new_counts[0] = nr_counts[1] 
	
	#print(sorted(nr_counts.items()))
	#print(sorted(new_probs.items()))
	return new_probs

	### End of Good Turing Backoff Calculation Function

def good_turing_backoff_smoothing_spellcheck(input_trigrams, n_grams):

	alpha1 = 0.5
	alpha2 = 0.5

	k = 6

	trigrams_count = n_grams[2]
	bigrams_count = n_grams[1]
	unigrams_count = n_grams[0]

	N_unigrams = sum(list(unigrams_count.values()))

	trigram_GTcounts = good_turing_backoff(trigrams_count, k) #Need to give k above which GTcounts will be the same
	bigram_GTcounts = good_turing_backoff(bigrams_count, k)

	probability = float(1)
	
	for trigram in input_trigrams:
		print(trigram)

		count_trigram = trigrams_count[trigram]

		#BackOff Algorithm
		if(count_trigram > 0): #if C(z|x,y) > 0
			probability_trigram = trigram_GTcounts[count_trigram] / float(bigrams_count[trigram[0:2]]) #P*(z|x,y)
		
		else if(bigrams_count[trigram[0:2]] > 0): #if C(x,y) > 0	
			if(bigrams_count[trigram[1:3]] > 0): #if C(y,z) > 0
				probability_bigram = bigram_GTcounts[bigrams_count[trigram[1:3]]] #P*(z|y)
			else:
				probability_bigram = alpha2 * ( unigrams_count[trigram[2]] / N_unigrams ) #alpha*P(z)
			
			probability_trigram = alpha1 * probability_bigram

		else:
			probability_trigram = unigrams_count[trigram[2]] / N_unigrams


		print(probability_trigram)
		probability = probability * probability_trigram;

	print(probability)
	return probability

	###End of Good Turing Backoff Spellcheck Function


def spellcheck(input_word, n_grams):
	#Generating probabilities for a given word
	input_trigrams = []

	#Converting input word to lowercase
	input_word = input_word.lower()

	#Dividing word into trigrams
	count = 0
	
	while(len(input_word) - count >= 3):
		input_trigrams.append(input_word[count:count+3])
		count = count + 1

	#print(input_trigrams)

	#probability = no_smoothing_spellcheck(input_trigrams, n_grams) #Uncomment to do no smoothing
	#probability = laplace_smoothing_spellcheck(input_trigrams, n_grams)
	probability = good_turing_smoothing_spellcheck(input_trigrams, n_grams)

	return probability

	###End of SpellCheck Function

def no_smoothing_grammar(input_trigrams, n_grams):
	#Calculating probabilities without any smoothing

	#Getting ngram counts
	bigrams_count = n_grams[1]
	trigrams_count = n_grams[2]

	#Calculating Probabilities
	probability = float(1)
	for trigram in input_trigrams:
		
		#We need P(word3|word1,word2) = Count(word3|word1,word2) / Count(word1,word2)

		#Need to convert list of words into one string so that we can search and get the counts
		trigram_new = '~'.join(trigram)
		bigram_new = '~'.join(trigram[0:2])

		probability_trigram = float(trigrams_count[trigram_new]) / float(bigrams_count[bigram_new])
		probability = probability * probability_trigram

	#print(probability)
	return probability

	###End of No Smoothing Function

def laplace_smoothing_grammar(input_trigrams, n_grams):
	#We need Vocabulary Size(V), i.e. number of possible trigrams
	
	unigrams_count = n_grams[0]
	bigrams_count = n_grams[1]
	trigrams_count = n_grams[2]
	
	C = len(unigrams_count) #Note that the words here will also include punctuations etc. which we can assume
							#will not be present in the input but this makes for a more general solution
	#print(C)
	
	V = float(C*C*C)				#This is the number of all possible trigrams with our available words

	#Calculating probabilities with Laplace Smoothing

	probability = float(1)
	
	for trigram in input_trigrams:
		
		#We need P(word3|word1,word2) = Count(word3|word1,word2) / Count(word1,word2)

		#Need to convert list of words into one string so that we can search and get the counts
		trigram_new = '~'.join(trigram)
		bigram_new = '~'.join(trigram[0:2])

		probability_trigram = (float(trigrams_count[trigram_new]) + 1) / (float(bigrams_count[bigram_new]) + V)
		probability = probability * probability_trigram

	print(probability) #If returned, it becomes 'None'
	#return probability

	###End of Laplace Smoothing function

def good_turing_smoothing_grammar(input_trigrams, n_grams):

	k = 6

	trigrams_count = n_grams[2]
	bigrams_count = n_grams[1]

	new_probs = good_turing(n_grams, k) #Need to give k above which GTcounts will be the same

	probability = float(1)
	
	for trigram in input_trigrams:
		#Need to convert list of words into one string so that we can search and get the counts
		trigram_new = '~'.join(trigram)
		#print(trigram_new)

		count_trigram = trigrams_count[trigram_new]

		GTprob = new_probs[count_trigram]

		#print(GTprob)
		probability = probability * GTprob;

	print(probability)
	return probability

	###End of Good Turing Grammar Function

def grammar_check(input_sentence, n_grams):
	#Generating probabilities for a given sentence
	input_trigrams = []

	#Tokenising the input with our tokeniser
	tokenised_input = tokenise(input_sentence)

	tokenised_input = tokenised_input[0] #Taking the first sentence recognised by the tokeniser --?Can be changed

	#print(tokenised_input)

	#LM for Grammar Check
	count = 0

	while(len(tokenised_input) - count >=3):
		input_trigrams.append(tokenised_input[count:count+3])
		count = count + 1

	#print(input_trigrams)

	#probability = no_smoothing_grammar(input_trigrams, n_grams) #Uncomment to do no smoothing
	#probability = laplace_smoothing_grammar(input_trigrams, n_grams) #Uncomment to do Laplace Smoothing
	probability = good_turing_smoothing_grammar(input_trigrams, n_grams)

	###End of Grammar Checking Function

#MAIN
tokenised = []

for i in os.listdir('/home/tanmai/Documents/NLP/Assignment1/Gutenberg/try'):
    if i.endswith('.txt'):
        file = open(i)
        text = file.read()

        tokenised = tokenised + tokenise(text) #Tokenising text and adding to one variable which stores tokens
        
        file.close()


#print(tokenised)
n_grams_spellcheck = language_model_spellcheck(tokenised)

print(spellcheck('hello', n_grams_spellcheck))
#print(spellcheck('olleh', n_grams_spellcheck))


n_grams_grammar = language_model_grammar(tokenised)

#grammar_check('I do not consider it necessary, at present, for me to discuss those matters', n_grams_grammar)
#grammar_check('do not I it consider at necessary,  present, me for to those discuss matters', n_grams_grammar)






