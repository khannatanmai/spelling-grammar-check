# spelling-grammar-check

Tokenises given input, creates Language models, Smoothing (Laplace, Good Turing, Back off) and gives a probability of correctness to the spelling of a word and the grammaticality of a sentence.